package sheridan;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class FahrenheitTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}


	@Test
	public void testConvertFromCelsius() {
		int result = Fahrenheit.convertFromCelsius(10);
		assertTrue("The temperature was not calculated properly", result == 50);
	}
	
	@Test
	public void testConvertFromCelsiusNegative() {
		int result = Fahrenheit.convertFromCelsius(20);
		assertFalse("The temperature was not calculated properly", result == 50);
	}

	@Test
	public void testConvertFromCelsiusBoundaryIn() {
		double result = Fahrenheit.convertFromCelsius(32);
		assertFalse("The temperature was not calculated properly", result == 89.6);
	}

	@Test
	public void testConvertFromCelsiusBoundaryOut() {
		double result = Fahrenheit.convertFromCelsius(32);
		assertFalse("The temperature was not calculated properly", result == 90.2);
	}


}
