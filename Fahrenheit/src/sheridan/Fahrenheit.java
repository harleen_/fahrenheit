package sheridan;

public class Fahrenheit {

	public static void main(String[] args) {
		
		System.out.println("The converted tempertaure is " + convertFromCelsius(10) + " degrees Fahrenheit");
	}
	
	public static int convertFromCelsius(int value) {
		double temp = ((value) * (9.0/5.0)) + 32;
		return (int) Math.round(temp);
	}

}
